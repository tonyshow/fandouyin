# uniapp-仿抖音短视频模版

#### 介绍
uniapp-仿抖音模版
请导入HbuilderX中，按照页面说明提示即可。

#### 安装教程
方式一
1.  git clone https://gitee.com/ZhenYJ/fandouyin.git
2.  引入HbuilderX中
3.  选择对应端（App、小程序、H5）页面启动运行程序即可

方式二
1.  复制链接打开uniapp插件市场
2.  直接导入HbuilderX
3.  链接：https://ext.dcloud.net.cn/plugin?id=5656
4.  选择对应端（App、小程序、H5）页面启动运行程序即可

#### 使用说明

# 🌟🌟🌟🌟🌟🌟🌟
【一定注意 -- start】
# 注意事项：

# 1.如果导入含 tabbar 页面，请在 onLoad()中修改 deleteHeigth的值。

# 2.index.nvue页面适用于：安卓、iOS。

# 3.nvueSwiper页面适用于：手机H5、小程序。（暂不支持PC）

# 4.小程序、H5用户导入插件demo后，勿直接运行，默认是App端运行，小程序、H5用户请参考视频教程。

## 5.小程序端：<测试阶段请按照如下配置信息>

> 1.前往小程序官网配置请求域名：https://bdb24c6d-8c19-4f80-8e7e-c9c9f037f131.bspapp.com【以免测试过程中出现测试数据无法请求的问题】
<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/5a365dbc-a3e3-4055-8904-72ffdae5c9ff.png" width="600px"/>

> 2.在开发者工具调试后，为保证性能可以启用真机预览(而非使用真机调试)，第一步配好以后才不会出现黑屏
<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/70fa80a7-9b25-4c5e-9e0c-8e5460d9d611.png" width="600px"/>
<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/176a1173-222a-4dfb-8d4a-e6026ca607d6.png" width="600px"/>

# 🌟🌟🌟🌟🌟🌟🌟
【一定注意 -- end】

># 至2022年4月29日，此插件已经连续更新和维护9个月。

># 对于如何提升播放性能，如何做到像抖音一样随滑随播，于是在github和gitee和DCloud插件市场和各个论坛，翻遍了播放插件和优化思路，很多插件发布以后都不会再进行长期维护，作者承诺此插件免费发布，并且长期维护，感谢大家对作者工作的支持。

># · 预加载：在线预加载、离线在线混合预加载

># · 优化播放、加载视频算法（可以无限刷视频）

># · 解决App端视频列表上下滑动、视频自动滑动播放的难点

># · 解决视频播放前黑屏的问题，解决其他短视频组件播放前还要等待还要加载，还要转圈的问题，做到了提前加载，即滑即播。
>### 注意：此插件原生实现，没有基于第三方原生插件，解决提前预播的难点，视频播放性能再次得到提升。

># 注意⚠️：所有页面都为 nvue 页面，请误引入至 vue 页面中。

># App端请勾选VideoPlayer模块。否则视频无法播放，切记，切记，切记。

[详细参考官方文档：<https://uniapp.dcloud.io/nvue-outline>]



1.插件可直接粘贴复制，不影响其他功能    

### 2.代码详解已经一步一步写在代码旁边了，优化了蛮多视频组件常见的一些问题.
* 比如：
```
滑着滑着卡死的.
还只是稍稍滑一点点就播放下一个视频或者上一个视频
滑动太快播放串音
for循环在低端机直接卡死等等。
```
* 这个版本都有纠正和改进。
* ### 欢迎大家多多指正。有不懂的问题可以在群里提出来。

# QQ群：953408573  

3.官方的进度条太臃肿而且无法更改，其他组件的进度条难分离，此插件精简了进度条代码，高度自定义进度条样式。


4.安卓测试机型： 
```
高端机型：华为 Mate40、芯片：麒麟9000、系统：Android 10.0  

低端机型：红米 Note3、芯片：高通骁龙650、系统：Android 6.0.1  

说明：高端机型得益于芯片，滑动流畅，其他如上说明。低端机型由于机器太老6年前的安卓手机，滑动太快的时候会卡住，但是过一会就会好。  
```

5.苹果测试机型：  
```
高端机型：iPhone12，iPad Pro2020、芯片：A14，A12Z、系统：iOS14.5、iPadOS 14.2  

低端机型：iPhone6、芯片：A8、系统：iOS 12.4  

说明：高端机型得益于芯片，滑动流畅，其他如上说明。低端机型：6年前的苹果产品还是够挺，滑动只是略微卡顿。  
```
6.演示视频链接：
### <https://vkceyugu.cdn.bspapp.com/VKCEYUGU-0455454d-b373-4768-aa39-dc1226fc1362/ea607407-c7d0-4219-adff-bcf6337ed1e8.mp4>

7.安卓体验链接：
### <https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/d01d4d28-aee0-4679-a20d-edba731e608b.apk>

### 8.App端插件引入了评论，这一部分仅做参考（如果需要H5、小程序请参考评论插件：<https://ext.dcloud.net.cn/plugin?id=7875>）
(App端引入)（uni-popup组件已经被改造了）
```
<uni-popup type="bottom" ref="pinglun" @touchmove.stop.prevent="moveHandle">
    <view :style="'width: '+ windowWidth +'px; height: '+ (boxStyle.height/heightNum) +'px; background-color: #242424; border-top-left-radius: 10px; border-top-right-radius: 10px;'">
        <!-- 
         这里就是App评论组件
         -->
        <douyin-scrollview
        :Width="windowWidth"
        :Height="(boxStyle.height/1.23)"
        :deleteIOSHeight="36"
        :deleteAndroidHeight="15"
        @closeScrollview="closeScrollview"
        ></douyin-scrollview>
    </view>
</uni-popup>
```

## 9.视频教程：
## <https://www.bilibili.com/video/BV1gi4y12745>

## 10.关于问题反馈：
> ## （1）最有效的方式就是加 QQ 群反馈，消息及时，解决速度快。

> ## （2）评论区反馈格式如下：
 (如下格式才可得到有效回复)
```
使用端：App[iOS\Android]（小程序、H5）
问题描述：xxxxx
其他内容：xxxxx
```
（示例）
```
使用端：App[iOS]
问题描述：如何处理安全区高度
其他内容：【配图1】【配图2】
```

# App端请勾选VideoPlayer模块。否则视频无法播放，切记，切记，切记。

### · 捐赠插件的开发

觉得不错的话快来支持一下作者吧

<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/25688f24-8b39-4128-8f62-08d75d5c9f3b.jpg" width="200px"/>
